import { createPublicKey } from "crypto";
import { ChangeEvent, useEffect, useState } from "react"

let defaultTasks = [newTask("Plan your day"),]

interface Task {
    text:string,
    done:boolean,
}

function newTask(t:string) {
    return { text: t, done:false, timeElapsed:0, running: false }
}

function StartStop(props:{task:Task, startStop:() => void, timeElapsed:number, running:boolean}) {
    if (props.task.done) {
        return <></>
    } else if(props.timeElapsed == 0) {
        return <button onClick={props.startStop}>Start</button>; 
    } else if(props.running) {
        return <button onClick={props.startStop}>Stop</button>; 
    } else {
        return <button onClick={props.startStop}>Continue</button>; 
    } 
}

function TaskItem(props:{task:Task, toggle:() => void}) {
    const style = props.task.done ? { textDecoration: 'line-through'} : {}

    const [state, setState] = useState({time:0, running:false})

    function tick() {
        console.log("tick")
        setState((state) => state.running ? {...state, time:state.time+1} : state )
    }

    function setup() {
        setInterval(tick, 1000)
    }
    useEffect(setup,[])

    function startStopFunction() {
        console.log(state.running)
        setState({...state,running:!state.running})
    }

    return <li style={style}>
        <span onClick={props.toggle}>{props.task.text} 
        ({state.time}) </span>
        <StartStop task={props.task} startStop={startStopFunction} timeElapsed={state.time} running={state.running}/>
        </li>
}

function ToDoList() {

    const [desc, setDesc] = useState("")
    const [tasks, setTasks] = useState(defaultTasks)

    function changeText(x:ChangeEvent<HTMLInputElement>) { setDesc(x.target.value) }

    function submit(e:any) {
        e.preventDefault()
        setTasks([...tasks, newTask(desc)])
        setDesc("")
    }

    function loadTasks() {
        fetch("/tasks.json")
        .then( data => data.json() )
        .then( tasks => setTasks(tasks))
    }

    useEffect(loadTasks,[])

    function toggleWithIndex(i:number) {
        setTasks(tasks.map( (t,j) => j == i ? {...t, done:!t.done} : t))
        // let newTasks = []
        // for (const j in tasks) {
        //     if( i.toString() == j ) {
        //         newTasks.push({ ...tasks[i], done:!tasks[i].done})
        //     } else {
        //         newTasks.push(tasks[i])
        //     }
        // }
        // setTasks(newTasks)
    }

    return <div>
        <p>{desc}</p>
        <ul>
            {tasks.map((t, i) => <TaskItem task={t} toggle={() => toggleWithIndex(i)}/>) }
        </ul>

        <form>
            <input type="text" onChange={changeText} value={desc}></input>
            <input type="submit" value="New Task" onClick={submit}></input>
        </form>
    </div>
}

export default ToDoList