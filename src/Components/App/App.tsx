import './App.css';
import ToDoList from '../ToDoList/ToDoList'
import { Container } from '@material-ui/core';

function App() {
  return (
    <Container>
    <header>Getting Things Done!</header>
    <section><ToDoList/> </section>
    <footer>Contacts, links</footer>
    </Container>
  );
}

export default App;
