import { useState, useEffect } from "react"

function Clock() {

    const t = useState(new Date())
    let date = t[0]
    let setDate = t[1]

    let hours = date.getHours()
    let minutes = date.getMinutes()
    let seconds = date.getSeconds()

    function tick() { setDate(new Date()) }

    function setup() { setInterval(tick, 1000) }

    function padding(s: number) { return s.toString().padStart(2, '0'); }

    useEffect( setup , [] ) // Only the first time 

    return <span>{padding(hours)}:{padding(minutes)}:{padding(seconds)}</span>
}

export default Clock; 