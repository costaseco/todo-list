import { ChangeEvent, useEffect, useState } from "react"
import { Button, TextField, List, ListItem, ListItemText, ListItemIcon, Checkbox, Container } from '@material-ui/core';
import { Add } from "@material-ui/icons";


interface Task { text:string, done: boolean}

function ToDoList() {

    const [desc, setDesc] = useState("")
    const [tasks, setTasks] = useState([] as Task[])

    function loadTasks() {
        fetch("/tasks")
        .then(data => data.json())
        .then(tasks => setTasks(tasks) )
    }
    useEffect( loadTasks, [] )

    function createTask(task:Task) {
        fetch("/tasks", {
            method: "POST", headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(task) })
        .then( data => console.log(data) )
    }

    function toggleOnServer(i: number) {
        fetch(`/tasks/${i}`, {
            method: "PUT", headers: {
                'content-type': 'application/json;charset=UTF-8',
            }
        })
        .then(data => console.log(data))
    }

    function changeText(x:ChangeEvent<HTMLInputElement>) {
        setDesc(x.target.value)
    }

    function submit(e:any) {
        e.preventDefault()
        let task = { text: desc, done: false }
        createTask(task)
        setTasks( [...tasks, task] )
        setDesc("")
    }

    function handleToggle(i:number ) {
        // Functional Javascript
        toggleOnServer(i)
        setTasks( tasks.map( (t,j) => j == i ? {...t, done:!t.done} : t ) )

        // Math: { { text: t.text, done:d } | t in tasks, d = idx(t) == i ? !t.done: t.done  }

        // Imperative Javascript
        // let newTasks = []

        // for (let j = 0; j < tasks.length; j++) {
        //     const task = tasks[j];
            
        //     if ( i == j ) {
        //         newTasks.push({...task, done: !task.done})
        //         console.log("completed "+i)
        //     } else {
        //         newTasks.push( task )
        //     }
        // }

        // setTasks(newTasks)
    }


    return <Container>
        <List>
            {
                tasks.map((t, i) => {
                    let x = t.done ? { textDecoration: 'line-through'} : {}

                    return <ListItem key={i} onClick={() => handleToggle(i)} style={x}>
                                <ListItemIcon>
                                    <Checkbox
                                        edge="start"
                                        checked={t.done}
                                        tabIndex={-1}
                                        disableRipple
                                    />
                                    </ListItemIcon>
                                    <ListItemText id="a">
                                    {t.text}
                                    </ListItemText>
                                </ListItem>
                })
            }
        </List>
        <form onSubmit={submit}>
            <TextField label="New Task Description" 
                       onChange={changeText} 
                       value={desc} 
                       InputLabelProps={{ shrink: true, }}/>
            { desc != "" && <Button color="primary" 
                    variant={"contained"} 
                    startIcon={<Add />} 
                    onClick={submit}>
                Add task
            </Button>}
        </form>
    </Container>
}

export default ToDoList